package com.mykolaviv.basecamp.soap.service.endpoints;

import com.mykolaviv.basecamp.soap.model.User;
import com.mykolaviv.basecamp.soap.exceptions.UserNotFoundException;
import com.mykolaviv.basecamp.soap.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.jws.WebParam;
import javax.jws.WebService;

@Slf4j
@WebService
public class GetUserById {

    @Inject
    private UserRepository userRepository;

    public User getUserById(@WebParam(name = "id") Integer id) throws UserNotFoundException {
        if (id == null) {
            throw new IllegalArgumentException("Id cannot be null");
        }
        User user = userRepository.findById(id);
        if (user == null) {
            throw new UserNotFoundException(id);
        }
        return user;
    }
}
