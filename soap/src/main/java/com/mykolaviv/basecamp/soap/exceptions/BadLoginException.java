package com.mykolaviv.basecamp.soap.exceptions;

public class BadLoginException extends Exception {
    public BadLoginException(String login) {
        super("Bad login: " + login);
    }
}
