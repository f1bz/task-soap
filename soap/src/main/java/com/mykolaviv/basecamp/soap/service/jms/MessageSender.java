package com.mykolaviv.basecamp.soap.service.jms;

import lombok.extern.slf4j.Slf4j;

import javax.enterprise.context.ApplicationScoped;
import javax.jms.MapMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.naming.InitialContext;

@Slf4j
@ApplicationScoped
public class MessageSender {

    public void sendMessage(String createdUserLogin, String createdUserPassword) {
        try {
            InitialContext initialContext = new InitialContext();
            QueueConnectionFactory connectionFactory = (QueueConnectionFactory) initialContext.lookup("java:/ConnectionFactory");
            QueueConnection queueConnection = connectionFactory.createQueueConnection();
            queueConnection.start();
            QueueSession queueSession = queueConnection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue queue = (Queue) initialContext.lookup("java:/jms/queue/test");
            QueueSender queueSender = queueSession.createSender(queue);
            MapMessage message = queueSession.createMapMessage();
            message.setString("login", createdUserLogin);
            message.setString("password", createdUserPassword);
            queueSender.send(message);
            queueConnection.close();
        } catch (Exception e) {
            log.error("ERROR HAPPENED: ", e);
        }
    }
}