package com.mykolaviv.basecamp.soap.repository;

import com.mykolaviv.basecamp.soap.model.User;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

@Stateless
public class UserRepository {

    @PersistenceContext(unitName = "MySqlDS1")
    private EntityManager entityManager;

    public void save(User user) {
        entityManager.persist(user);
    }

    public User findById(int id) {
        return entityManager.find(User.class, id);
    }

    public User findByLogin(String login) {
        try {
            return (User) entityManager.createQuery("SELECT t FROM User t where t.login = :login")
                    .setParameter("login", login).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}