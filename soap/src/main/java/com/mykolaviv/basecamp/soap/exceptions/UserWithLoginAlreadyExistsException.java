package com.mykolaviv.basecamp.soap.exceptions;

public class UserWithLoginAlreadyExistsException extends Exception {

    public UserWithLoginAlreadyExistsException(String login) {
        super("User already exists with login: " + login);
    }
}