package com.mykolaviv.basecamp.soap.exceptions;

public class UserNotFoundException extends Exception {

    public UserNotFoundException(int id) {
        super("User not found with id: " + id);
    }
}