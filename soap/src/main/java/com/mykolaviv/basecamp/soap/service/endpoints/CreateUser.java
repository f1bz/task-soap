package com.mykolaviv.basecamp.soap.service.endpoints;

import com.mykolaviv.basecamp.soap.exceptions.BadLoginException;
import com.mykolaviv.basecamp.soap.model.User;
import com.mykolaviv.basecamp.soap.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import com.mykolaviv.basecamp.soap.exceptions.UserWithLoginAlreadyExistsException;
import com.mykolaviv.basecamp.soap.service.jms.MessageSender;

import javax.inject.Inject;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.regex.Pattern;

@Slf4j
@WebService
public class CreateUser {

    @Inject
    private UserRepository userRepository;

    @Inject
    private MessageSender messageSender;

    private static final Pattern LOGIN_PATTERN = Pattern.compile("[a-zA-Z-0-9]{5,}");

    public int createUser(@WebParam(name = "login") String login,
                          @WebParam(name = "password") String password,
                          @WebParam(name = "age") Integer age,
                          @WebParam(name = "country") String country) throws UserWithLoginAlreadyExistsException, BadLoginException {
        if (login == null || login.isEmpty()) {
            throw new IllegalArgumentException("Login cannot be null");
        }
        if (password == null || password.isEmpty()) {
            throw new IllegalArgumentException("Password cannot be null or empty");
        }
        if (country == null || country.isEmpty()) {
            throw new IllegalArgumentException("Country cannot be null or empty");
        }
        if (age == null) {
            throw new IllegalArgumentException("Age cannot be null");
        }

        if (!LOGIN_PATTERN.matcher(login).matches()) {
            throw new BadLoginException(login);
        }
        User user = new User(login, password, country, age);
        if (userRepository.findByLogin(login) != null) {
            throw new UserWithLoginAlreadyExistsException(login);
        }
        userRepository.save(user);
        messageSender.sendMessage(user.getLogin(), user.getPassword());
        return user.getId();
    }
}
