package com.mykolaviv.basecamp.soap.service;

import lombok.extern.slf4j.Slf4j;

import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

@Slf4j
public class EmailSender {

    private static final String RECEIVER_EMAIL = "fibbyua@gmail.com";
    private static final String SENDER_EMAIL = "fibbyua@gmail.com";
    private static final String SENDER_PASSWORD = "password";

    public void sendEmail(String textToSend) throws MessagingException, UnsupportedEncodingException {
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        Session session = Session.getInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(SENDER_EMAIL, SENDER_PASSWORD);
            }
        });
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(RECEIVER_EMAIL, "User"));
        message.setRecipients(javax.mail.Message.RecipientType.TO, InternetAddress.parse(RECEIVER_EMAIL));
        message.setHeader("Content-Type", "text/plain; charset=UTF-8");
        message.setSubject(null, "UTF-8");
        Multipart multipart = new MimeMultipart();
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(textToSend, "text/html");
        multipart.addBodyPart(messageBodyPart);
        message.setContent(multipart, "UTF-8");
        Transport.send(message);
        log.info("Email was send!");
    }
}
