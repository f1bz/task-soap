package com.mykolaviv.basecamp.soap.service.jms;

import com.mykolaviv.basecamp.soap.service.EmailSender;
import lombok.extern.slf4j.Slf4j;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

@Slf4j
@MessageDriven(activationConfig = {
        @ActivationConfigProperty(
                propertyName = "destination",
                propertyValue = "java:/jms/queue/test"),
        @ActivationConfigProperty(propertyName = "acknowledgeMode",
                propertyValue = "Auto-acknowledge"),
        @ActivationConfigProperty(
                propertyName = "destinationType",
                propertyValue = "javax.jms.Queue")
})
public class MessageReceiver implements MessageListener {

    private final EmailSender emailSender = new EmailSender();

    @Override
    public void onMessage(Message message) {
        try {
            MapMessage mapMessage = (MapMessage) message;
            String login = mapMessage.getString("login");
            String password = mapMessage.getString("password");
            String textToSend = login + " | " + password;
            log.info(String.format("Message received: %s", textToSend));
            emailSender.sendEmail(textToSend);
        } catch (Exception e) {
            log.error("ERROR HAPPENED: ", e);
        }
    }


}